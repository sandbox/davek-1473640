<div id="dialog-purchase-confirm" title="Purchase this photo?">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you want to purchase this photo?</p>
</div>

<div id="dialog-already-purchased" title="Already purchased">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This image has already been purchased.</p>
</div>