<?php

class BigstockMediaPlugin extends MediaBrowserPlugin {

	function view() {
		$path = drupal_get_path('module', 'bigstock_media');

		$attached = array();
		$attached['css'] = array($path . '/css/bigstock_media.css');
		$attached['js'] = array($path . '/js/bigstock_media.js');
		
		return array(
			'header' => array(
				'#markup' => theme('bigstock_media_header'),
			),
			'purchased' => array(
				'#prefix' => '<div id="bigstock_media-screen-wrapper"><div id="bigstock_media-screen-purchased">',
				'#suffix' => '</div>',
				'#markup' => theme('bigstock_media_purchased', array(
					'table' => bigstock_media_purchased_table(),
				)),
			),
			'search' => array(
				'#prefix' => '<div id="bigstock_media-screen-search">',
				'#suffix' => '</div></div>',
				'form' => drupal_get_form('bigstock_media_search_form'),
			),
			'#attached' => $attached,
		);
	}

}