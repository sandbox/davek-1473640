<?php
/*
Online documentation available:
	http://help.bigstockphoto.com/entries/20843622-api-overview

*/

class BspApiException extends Exception {

}

class BspApi {
	const VERSION_MAJOR		= 2;
	const VERSION_MINOR		= 0;
	protected $account_id	= '';
	protected $secret_key	= '';
	protected $mode			= 'test';
	private $base_url		= '';
	public $response_format	= 'json';
	
	public static $domains	= array(
		'test'	=> 'testapi.bigstockphoto.com',
		'prod'	=> 'api.bigstockphoto.com'
	);

	public static $curl_opts = array(
		CURLOPT_CONNECTTIMEOUT => 10,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_TIMEOUT        => 60,
		CURLOPT_USERAGENT      => 'bspapi-php-',
	);
	
	public function __construct($config) {
		$this->account_id	= $config['account_id'];
		$this->secret_key	= $config['secret_key'];
		if ( isset($config['mode']) && $config['mode']=='prod' ) {
			$this->setProdMode();
		} else {
			$this->setTestMode();
		}
		self::$curl_opts[CURLOPT_USERAGENT]	.= self::VERSION_MAJOR . '.' . self::VERSION_MINOR;
	}
	
	public function setTestMode() {
		$this->base_url		= self::$domains['test'] . '/' . self::VERSION_MAJOR . '/' . $this->account_id . '/';
	}

	public function setProdMode() {
		$this->base_url		= self::$domains['prod'] . '/' . self::VERSION_MAJOR . '/' . $this->account_id . '/';
	}
	
	public function search($params) {
		if ( count($params) == 0 ) {
			throw new BspApiException('No Search Parameters passed: '.implode(', ',$invalid_params) );
		}
		// Check of a response format was specified
		if ( !isset($params['format']) ) {
			$params['format']		= $this->response_format;
		}
		// Build search URL
		$search_url			= 'http://' . $this->base_url.'search/?' . http_build_query($params);
		return $this->sendRequest($search_url);
		
	}
	
	public function image($image_id, $fields=null) {
		$params			= array('image_id'=>$image_id);
		if ( !is_null($fields) ) {
			$params['fields']	= $fields;
		}
		$image_url				= 'https://' . $this->base_url.'image/' . $image_id;
		return $this->sendRequest($image_url);
	}
	
	public function purchase($image_id, $format_id) {
		$auth_key		= sha1($this->secret_key . $this->account_id . $image_id);
		$params			= array(
			'image_id'	=> $image_id,
			'format_id'	=> $format_id,
			'auth_key'	=> $auth_key,
			'agreed'	=> true
		);
		$image_url		= 'https://' . $this->base_url.'purchase/?' . http_build_query($params);
		return $this->sendRequest($image_url);
	}
	
	public function getDownloadUrl($download_id) {
		$auth_key		= sha1($this->secret_key . $this->account_id . $download_id);
		$params			= array(
			'download_id'	=> $download_id,
			'auth_key'	=> $auth_key,
			'agreed'	=> "true"
		);
		$download_url		= 'http://' . $this->base_url.'download/?' . http_build_query($params);
		return $download_url;
	}
	
	protected function sendRequest($url) {
		$ch					= curl_init();
		$opts				= self::$curl_opts;
		$opts[CURLOPT_URL]	= $url;

		// test
		$opts[CURLOPT_SSL_VERIFYPEER]	= 0;
		
		curl_setopt_array($ch, $opts);
		$result				= curl_exec($ch);
		if ($result === false) {
			$err_no		= curl_errno($ch);
			$err_msg	= 'SendRequest Exception: '.curl_error($ch);
			curl_close($ch);
			throw new BspApiException($err_msg, $err_no);
		}
		curl_close($ch);
		return $result;
	}

}
