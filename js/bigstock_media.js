(function ($) {
	namespace('Drupal.bigstock_media');

	Drupal.behaviors.bigstock_media = {
		attach: function(context) {
			// attach() gets called multiple times after an ajax refresh for some reason - hence the undelegate()
			$('#bsp-search-results').undelegate('.bsp-search-result').delegate('.bsp-search-result', 'click', function(event) {
				var bsp_id = $(event.currentTarget).data('bsp-id');
				$("#dialog-purchase-confirm").dialog({
					resizable: false,
					height:180,
					modal: true,
					buttons: {
						Purchase: function() {
							var self = this;
							Drupal.bigstock_media.purchase(bsp_id, function(data) {
								$(self).dialog('close');
								if (!data.purchased) {
									Drupal.media.browser.selectMedia([{
										fid: data.fid
									}]);
									Drupal.media.browser.submit();
								} else {
									$("#dialog-already-purchased").dialog({
										modal: true,
										buttons: {
											Ok: function() {
												$(this).dialog("close");
											}
										}
									});
								}
							});
						},
						Cancel: function() {
							$(this).dialog('close');
						}
					}
				});
			});

			$('#bsp-purchased').undelegate('.bsp-purchased-thumbnail').delegate('.bsp-purchased-thumbnail', 'click', function(event) {
				Drupal.media.browser.selectMedia([{
					fid: $(event.currentTarget).data('fid')
				}]);
				Drupal.media.browser.submit();
			});

			$('#bigstock_media-btn-purchased').click(function(event) {
				$('#bigstock_media-screen-purchased').show();
				$('#bigstock_media-screen-search').hide();
				$('#bigstock_media-btn-purchased').addClass('active')
				$('#bigstock_media-btn-search').removeClass('active');
			});

			$('#bigstock_media-btn-search').click(function(event) {
				$('#bigstock_media-screen-search').show();
				$('#bigstock_media-screen-purchased').hide();
				$('#bigstock_media-btn-search').addClass('active');
				$('#bigstock_media-btn-purchased').removeClass('active');
			});
		}
	};

	Drupal.bigstock_media.ajax = function(action, data, callback) {
		$.post(Drupal.settings.basePath + 'media/bigstock_media/ajax/' + action, data, callback);
	};

	Drupal.bigstock_media.purchase = function(bsp_id, callback) {
		Drupal.bigstock_media.ajax('check_purchased', {id: bsp_id}, function(data) {
			if (!data.purchased) {
				Drupal.bigstock_media.ajax('purchase', {id: bsp_id}, callback);
			} else {
				callback(data);
			}
		});
	};

})(jQuery);